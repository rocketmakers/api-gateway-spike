#############################
# VARIABLES
#############################
START 										:= $(shell printf "\033[34;1m")
END 											:= $(shell printf "\033[0m")

PROJECT_NAME=cosmos-cloud-307717
PROJECT=cosmos-cloud-tester
NODE_MODULES=./node_modules
TS_RUN_BUILD=$(NODE_MODULES)/ts-node/dist/bin.js -P ./_build/tsconfig.json

#############################
# INFRASTRUCTURE
#############################
INFRASTRUCTURE_NODE_MODULES=./infrastructure/node_modules
INFRASTRUCTURE_TS_NODE = $(INFRASTRUCTURE_NODE_MODULES)/ts-node/dist/bin.js
INFRASTRUCTURE_TERRAFORM_SCRIPT = ./infrastructure/src/run/terraform.ts

#############################
# SCRIPTS
#############################
LINT=$(NODE_MODULES)/eslint/bin/eslint.js . --max-warnings=0 --ext ts --ext tsx -c ./.eslintrc.js
FORMAT=$(NODE_MODULES)/prettier/bin-prettier.js -c .prettierrc.yaml --write '**/*.ts?(x)'
TSC=$(NODE_MODULES)/typescript/bin/tsc -b -v
COMMITIZEN=$(NODE_MODULES)/commitizen/bin/git-cz

TF_COMMAND ?= plan

#############################
# CUSTOM FUNCTIONS
#############################
define header
  $(info $(START)▶▶▶ $(1)$(END))
endef

#############################
# BASE COMMANDS
#############################
install:
	$(call header,Installing...)
	npm install

#############################
# INFRASTRUCTURE COMMANDS
#############################
api-gateway-infrastructure:
	$(call header,Gateway Infrastructure $(TF_COMMAND)...)
	$(INFRASTRUCTURE_TS_NODE) $(INFRASTRUCTURE_TERRAFORM_SCRIPT) --module=api-gateway -e=api-gateway -c=$(TF_COMMAND) -f
