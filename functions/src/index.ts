import * as functions from 'firebase-functions';

export const getUsers = functions.https.onRequest((_, response) => {
  response.send("Got Users");
});

export const getAdmins = functions.https.onRequest((_, response) => {
  response.send("Got Admins");
});
