# Api Gateway Spike

## Overview

Using GCP's Api Gateway module to combine multiple firebase functions and Cloud Run API methods in to one endpoint.

![infra.png](./infra.png)

In this spike we use two Cloud Functions.

The spec.yaml file contains the OpenApi config that configure the gateway.

```swagger: '2.0'
info:
  title: Spike Gateway
  description: API Gateway
  version: 1.0.0
schemes:
  - https
produces:
  - application/json
paths:
  /v1/getAdmins:
    get:
      summary: Get Admins
      operationId: get-admins
      x-google-backend:
        address: https://us-central1-cosmos-cloud-307717.cloudfunctions.net/getAdmins
      responses:
        '200':
          description: OK
  /v1/getUsers:
    get:
      summary: Get Users
      operationId: get-users
      x-google-backend:
        address: https://us-central1-cosmos-cloud-307717.cloudfunctions.net/getUsers
      responses:
        '200':
          description: OK
```

With a bit more work this document could be used to generate an api client.

This might be a good candidate to generate the initial document [https://www.npmjs.com/package/express-oas-generator](https://www.npmjs.com/package/express-oas-generator)

To test go to 

[https://gateway-71ehfiaz.ew.gateway.dev/v1/getUsers](https://gateway-71ehfiaz.ew.gateway.dev/v1/getUsers)

or

[https://gateway-71ehfiaz.ew.gateway.dev/v1/getAdmins](https://gateway-71ehfiaz.ew.gateway.dev/v1/getAdmins)

## Infrastructure

[Terraform](https://www.terraform.io/) is used to create and manage cloud
infrastructure.

We use [tfenv](https://github.com/tfutils/tfenv) to manage the terraform version.

```bash
# Change to infrastructure and install the configured terraform version
cd infrastructure
tfenv install
```

The [jq](https://stedolan.github.io/jq/) package is required by some scripts for parsing JSON on the command line:

We use TypeScript to run terraform so you need to install dependencies with `npm i` from the repository root.

## Terraform scripts

Each sub-directory of `infrastructure` (except `src` and `modules`) contains terraform scripts for one part of the cosmos-cloud infrastructure:

## Order of play

Please run the terraform modules in the following order:

1. api-gateway

## Commands

```bash
make api-gateway-infrastructure TF_COMMAND=plan
make api-gateway-infrastructure TF_COMMAND=apply
```
