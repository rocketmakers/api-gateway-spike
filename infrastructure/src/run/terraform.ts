#!/usr/bin/env ts-node

import { join } from 'path';
import { LoggerLevel } from '@rocketmakers/log';
import { Prerequisites } from '@rocketmakers/shell-commands/lib/prerequisites';
import { setDefaultLoggerLevel, createLogger } from '@rocketmakers/shell-commands/lib/logger';
import { Args } from '@rocketmakers/shell-commands/lib/args';
import { FileSystem } from '@rocketmakers/shell-commands/lib/fs';
import { terraform, TSupportedTerraformCommands } from '../modules/terraform';

async function run() {
  const validModules = FileSystem.getFolders(join(__dirname, '../..'))
    .filter(f => !['node_modules', 'modules', 'src'].includes(f.name))
    .map(f => f.name);
  const validLogLevels: LoggerLevel[] = ['trace', 'debug', 'info', 'warn', 'error', 'fatal'];
  const validTerraformCommands: TSupportedTerraformCommands[] = [
    'plan',
    'plan-and-apply',
    'apply',
    'destroy',
    'validate',
  ];

  const args = await Args.match({
    env: Args.single({
      description: 'The deployment environment',
      shortName: 'e',
      mandatory: true,
    }),
    command: Args.single({
      description: 'The terraform command to run',
      shortName: 'c',
      mandatory: true,
      validValues: validTerraformCommands,
    }),
    module: Args.single({
      description: 'The terraform module to deploy',
      shortName: 'm',
      mandatory: true,
      validValues: validModules,
    }),
    log: Args.single({
      description: 'The log level',
      shortName: 'l',
      defaultValue: 'info',
      validValues: validLogLevels,
    }),
    force: Args.switched({
      description: 'Force the command',
      shortName: 'f',
    }),
  });

  if (!args) {
    return;
  }

  setDefaultLoggerLevel(args.log as LoggerLevel);
  const logger = createLogger('run-terraform');

  try {
    await Prerequisites.check();

    const { command, module, env } = args;

    await terraform(command as TSupportedTerraformCommands, module, env, logger, { force: args.force });
  } catch (err) {
    logger.error(err);
    process.exit(-1);
  }
}

// eslint-disable-next-line @typescript-eslint/no-floating-promises
run();
