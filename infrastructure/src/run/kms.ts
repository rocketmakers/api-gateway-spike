#!/usr/bin/env ts-node

import { LoggerLevel } from '@rocketmakers/log';
import { Prerequisites } from '@rocketmakers/shell-commands/lib/prerequisites';
import { setDefaultLoggerLevel, createLogger } from '@rocketmakers/shell-commands/lib/logger';
import { Args } from '@rocketmakers/shell-commands/lib/args';
import { TSupportedKMSCommands, kms } from '../modules/kms';

async function run() {
  const validLogLevels: LoggerLevel[] = ['trace', 'debug', 'info', 'warn', 'error', 'fatal'];
  const validKMSCommand: TSupportedKMSCommands[] = ['encrypt', 'decrypt'];

  const args = await Args.match({
    command: Args.single({
      description: 'The command to run',
      shortName: 'c',
      mandatory: true,
      validValues: validKMSCommand,
    }),
    value: Args.single({
      description: 'The value to encrypt/decrypt',
      shortName: 'v',
      // mandatory: true,
    }),
    projectId: Args.single({
      description: 'The Google Cloud project ID',
      shortName: 'p',
      mandatory: true,
    }),
    region: Args.single({
      description: 'The Google Cloud region',
      shortName: 'r',
      defaultValue: 'europe-west1',
    }),
    key: Args.single({
      description: 'The Google Cloud KMS Crypto key',
      shortName: 'k',
      mandatory: true,
    }),
    keyRing: Args.single({
      description: 'The Google Cloud KMS Key ring',
      shortName: 'kr',
      mandatory: true,
    }),
    log: Args.single({
      description: 'The log level',
      shortName: 'l',
      defaultValue: 'info',
      validValues: validLogLevels,
    }),
    unix: Args.switched({
      description: 'If operating system is unix based',
      shortName: 'u',
    }),
  });

  if (!args) {
    return;
  }

  setDefaultLoggerLevel(args.log as LoggerLevel);
  const { command, value, projectId, region, key, keyRing, unix } = args;
  const logger = createLogger(`run-kms-${command}`);

  try {
    await Prerequisites.check();
    await kms(command as TSupportedKMSCommands, value, logger, { projectId, key, keyRing, region, unix });
  } catch (err) {
    logger.error(err);
    process.exit(-1);
  }
}

// eslint-disable-next-line @typescript-eslint/no-floating-promises
run();
