export function assertNever(_: never, message: string) {
  throw new Error(message);
}
