import { Logger } from '@rocketmakers/log';
import { Shell } from '@rocketmakers/shell-commands/lib/shell';
import { assertNever } from './helpers';

export type TSupportedKMSCommands = 'encrypt' | 'decrypt';

export interface IKmsOptions {
  projectId: string;
  region: string;
  keyRing: string;
  key: string;
  unix: boolean;
}

export async function kms(command: TSupportedKMSCommands, value: string, logger: Logger, options: IKmsOptions) {
  const baseCommand = ['gcloud', 'kms'];
  const flags = [
    '--project',
    options.projectId,
    '--location',
    options.region,
    '--keyring',
    options.keyRing,
    '--key',
    options.key,
    '--plaintext-file',
    '-',
    '--ciphertext-file',
    '-',
  ];
  switch (command) {
    case 'encrypt': {
      logger.info(`Encrypting`);
      const output = await Shell.execOutput('bash', [
        '-c',
        `echo ${value} | ${[...baseCommand, command, ...flags].join(' ')} | base64`,
      ]);
      logger.info('===============================');
      logger.info(`CIPHERTEXT: ${output}`);
      logger.info('===============================');
      break;
    }
    case 'decrypt': {
      logger.info('Decrypting');
      const output = await Shell.execOutput('bash', [
        '-c',
        `echo "${value}" | base64 -${options.unix ? 'D' : 'd'} | ${[...baseCommand, command, ...flags].join(' ')}`,
      ]);
      logger.info('===============================');
      logger.info(`PLAINTEXT: ${output}`);
      logger.info('===============================');
      break;
    }
    default: {
      assertNever(command, `Unsupported command type: ${command}`);
    }
  }
}
