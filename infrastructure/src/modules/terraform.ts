import { FileSystem } from '@rocketmakers/shell-commands/lib/fs';
import { Terraform } from '@rocketmakers/shell-commands/lib/terraform';
import { Logger } from '@rocketmakers/log';
import { join } from 'path';

export type TSupportedTerraformCommands = 'plan' | 'plan-and-apply' | 'apply' | 'destroy' | 'validate';

export interface ITerraformOptions {
  force?: boolean;
}

export function getVarFiles(modulePath: string, env: string) {
  const sharedVarsPath = join(modulePath, 'envs', 'shared.tfvars');
  if (!FileSystem.exists(sharedVarsPath)) {
    throw new Error(`Shared vars file does not exist at ${sharedVarsPath}`);
  }
  const envVarsPath = join(modulePath, 'envs', env, 'vars.tfvars');
  if (!FileSystem.exists(envVarsPath)) {
    throw new Error(`Env vars file does not exist at ${envVarsPath}`);
  }
  return [sharedVarsPath, envVarsPath];
}

export async function terraform(
  command: TSupportedTerraformCommands,
  module: string,
  env: string,
  logger: Logger,
  options?: ITerraformOptions
) {
  if (command === 'destroy' && !options?.force) {
    throw new Error('must force when destroying');
  }

  logger.info(`Checking existence of ${module}`);
  const modulePath = join(__dirname, '../..', module);
  if (!FileSystem.exists(modulePath)) {
    throw new Error(`terraform module ${module} does not exist`);
  }

  // Init
  logger.info(`Setup terraform`);
  await Terraform.init(modulePath);

  // Get var files
  logger.info(`Get var files`);
  const varFiles = getVarFiles(modulePath, env);

  // Select workspace
  try {
    logger.info(`Select workspace`);
    await Terraform.selectWorkspace(modulePath, env);
  } catch (err) {
    logger.info(`Didn't find workspace, try to create and select...`);
    await Terraform.newWorkspace(modulePath, env);
    await Terraform.selectWorkspace(modulePath, env);
  }

  // Format
  logger.info(`Formatting terraform`);
  await Terraform.format(modulePath);

  // Validate
  logger.info(`Validating terraform`);
  await Terraform.validate(modulePath);

  // Plan
  const planFilePath = join(modulePath, `${module}-${env}.tfplan`);
  if (command === 'plan' || command === 'plan-and-apply') {
    logger.info(`Run terraform plan`);
    await Terraform.plan(modulePath, planFilePath, {
      varFiles,
    });
  }

  // Apply
  if (command === 'apply' || command === 'plan-and-apply') {
    logger.info(`Run terraform apply`);
    if (!FileSystem.exists(planFilePath)) {
      throw new Error(`Plan file not found at ${planFilePath}`);
    }
    await Terraform.applyPlan(modulePath, planFilePath);
  }

  // Destroy
  if (command === 'destroy') {
    logger.info(`Run terraform destroy`);
    await Terraform.destroy(modulePath, { varFiles });
  }
}
