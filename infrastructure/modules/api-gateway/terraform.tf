terraform {
  required_version = "~> 0.12.13"
  required_providers {
    google = "~> 3.59.0"
    google-beta = "~> 3.59.0"
  }
}
