# cloud-nat

TODO: What is this for?

{{{ this.coreContent }}}

## Example Use Cases

```
module cloud_nat {
  source = "git::ssh://git@gitlab.com/rocketmakers/infrastructure/terraform-rocketmakers-modules.git//gcp/cloud-nat?ref=v0.8.0"

  enabled    = var.enable_cloud_nat
  project_id = var.project-id
  region     = var.region
  name       = terraform.workspace
  router     = terraform.workspace
  network    = terraform.workspace
}
```
