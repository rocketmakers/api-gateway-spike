# cloud-nat

TODO: What is this for?

## Required Inputs

| name    | description          | type   |
| ------- | -------------------- | ------ |
| `project_id` | The project ID to deploy to | string |
| `region` | The region to deploy to (e.g. europe-west1) | string |
| `router` | The name of the router in which this NAT will be configured. Changing this forces a new NAT to be created. | string |

## Optional Inputs

| name    | description          | type   | default value   |
| ------- | -------------------- | ------ | --------------- |
| `create_router` | Create router instead of using an existing one, uses 'router' variable for new resource name. | bool | true |
| `enabled` | Enable the module | bool | true |
| `icmp_idle_timeout_sec` | Timeout (in seconds) for ICMP connections. Defaults to 30s if not set. Changing this forces a new NAT to be created. | number | 30 |
| `min_ports_per_vm` | Minimum number of ports allocated to a VM from this NAT config. Defaults to 64 if not set. Changing this forces a new NAT to be created. | number | 64 |
| `name` | Defaults to 'cloud-nat-RANDOM_SUFFIX'. Changing this forces a new NAT to be created. | string |  |
| `nat_ip_allocate_option` | Value inferred based on nat_ips. If present set to MANUAL_ONLY, otherwise AUTO_ONLY. | bool | false |
| `network` | VPN name, only if router is not passed in and is created by the module. | string |  |
| `not-enabled-message` | Message indicating that the module isn't enabled | string | CLOUD_NAT_NOT_ENABLED |
| `router_asn` | Router ASN, only if router is not passed in and is created by the module. | number | 64514 |
| `source_subnetwork_ip_ranges_to_nat` | Defaults to ALL_SUBNETWORKS_ALL_IP_RANGES. How NAT should be configured per Subnetwork. Valid values include: ALL_SUBNETWORKS_ALL_IP_RANGES, ALL_SUBNETWORKS_ALL_PRIMARY_IP_RANGES, LIST_OF_SUBNETWORKS. Changing this forces a new NAT to be created. | string | ALL_SUBNETWORKS_ALL_IP_RANGES |
| `tcp_established_idle_timeout_sec` | Timeout (in seconds) for TCP established connections. Defaults to 1200s if not set. Changing this forces a new NAT to be created. | number | 1200 |
| `tcp_transitory_idle_timeout_sec` | Timeout (in seconds) for TCP transitory connections. Defaults to 30s if not set. Changing this forces a new NAT to be created. | number | 30 |
| `udp_idle_timeout_sec` | Timeout (in seconds) for UDP connections. Defaults to 30s if not set. Changing this forces a new NAT to be created. | number | 30 |

## Outputs

| name      | description                 |
| --------- | --------------------------- |
| `nat_ip` | NAT IP address |

## Requirements

These are required by the module.

| name | version |
| ---- | ------- |
| `google` | ~> 2.20 |
| `random` | ~> 2.2 |
| `terraform` | ~> 0.12.13 |

## Providers

These are the providers used by the module.

| name | version |
| ---- | ------- |
| `google` | ~> 2.20 |
| `random` | ~> 2.2 |


## Example Use Cases

```
module cloud_nat {
  source = "git::ssh://git@gitlab.com/rocketmakers/infrastructure/terraform-rocketmakers-modules.git//gcp/cloud-nat?ref=v0.8.0"

  enabled    = var.enable_cloud_nat
  project_id = var.project-id
  region     = var.region
  name       = terraform.workspace
  router     = terraform.workspace
  network    = terraform.workspace
}
```
