provider google-beta {
  version     = "3.59.0"
  project     = var.project_id
  region      = var.region
}

resource "google_project_service" "api_gateway_service" {
  count = var.enable_services ? 1 : 0
  service                    = "apigateway.googleapis.com"
  project                    = var.project_id
  disable_dependent_services = true
}

resource "google_project_service" "servicemanagement_service" {
  count = var.enable_services ? 1 : 0
  service                    = "servicemanagement.googleapis.com"
  project                    = var.project_id
  disable_dependent_services = true
}

resource "google_project_service" "servicecontrol" {
  count = var.enable_services ? 1 : 0
  service                    = "servicecontrol.googleapis.com"
  project                    = var.project_id
  disable_dependent_services = true
}
 
resource "google_api_gateway_api" "api_gateway" {
  provider     = google-beta
  api_id       = var.api_gateway_container_id
  display_name = var.gateway_display_name
  depends_on = [
    google_project_service.api_gateway_service
  ]
}

resource "google_api_gateway_api_config" "api_cfg" {
  provider      = google-beta
  api           = google_api_gateway_api.api_gateway.api_id
  api_config_id_prefix = var.gateway_config_prefix
  display_name  = "${var.gateway_display_name} Config"

  openapi_documents {
    document {
      path     = "spec.yaml"
      contents = filebase64("spec.yaml")
    }
  }
  depends_on = [
    google_project_service.api_gateway_service
  ]
}

resource "google_api_gateway_gateway" "gateway" {
  provider   = google-beta
  region     = var.region

  api_config   = google_api_gateway_api_config.api_cfg.id

  gateway_id   = var.gateway_id
  display_name = "The Gateway"

  depends_on = [google_api_gateway_api_config.api_cfg]
}