variable project_id {
  type        = string
  description = "The project ID to deploy to"
}

variable gateway_display_name {
  type        = string
  description = "Display name of the Gateway"
}

variable gateway_id {
  type        = string
  description = "Id of the Gateway"
}

variable gateway_config_prefix {
  type        = string
  description = "Id of the Gateway"
}

variable api_gateway_container_id {
  type        = string
  description = "Id of the Gateway Container"
}

variable region {
  type        = string
  description = "The region to deploy to (e.g. europe-west1)"
}

variable enable_services {
  type        = bool
  description = "Whether the module manages the required services"
}
