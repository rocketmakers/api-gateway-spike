variable project_id {
  type        = string
  description = "Google Cloud project ID"
  default     = "rocketmakers-developers"
}

variable region {
  type        = string
  description = "Google Cloud region"
  default     = "europe-west1"
}

variable zone {
  type        = string
  description = "Google Cloud zone"
  default     = "europe-west1-b"
}

variable project_name {
  type        = string
  description = "Project name"
}

variable remote_state_workspace {
  type        = string
  description = "Specify remote state workspace"
  default     = ""
}