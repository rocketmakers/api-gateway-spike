

module api-gateway {
  source                   = "../modules/api-gateway"
  enable_services          = true
  api_gateway_container_id = "mygateway"
  gateway_config_prefix    = "gw"
  gateway_id               = "gateway"
  region                   = var.region
  project_id               = var.project_id
  gateway_display_name     = "New Gateway"
}
