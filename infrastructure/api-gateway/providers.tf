provider google-beta {
  version = "3.59.0"
  project = var.project_id
  region  = var.region
}

provider google {
  version = "3.59.0"
  project = var.project_id
  region  = var.region
}

data google_client_config current {}

