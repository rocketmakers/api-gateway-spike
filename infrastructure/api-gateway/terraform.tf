terraform {
  required_version = "0.12.24"

  backend gcs {
    bucket = "cosmos-cloud-core"
    prefix = "infrastructure/cosmos-cloud-api-gateway/terraform/state"
  }
}
